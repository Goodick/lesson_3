using UnityEngine;

namespace Game.Core
{
    [CreateAssetMenu(menuName = "Data")]
    public class Data : ScriptableObject
    {
        [field: Header("Player"), SerializeField, Min(0)] public float PlayerSpeed { get; private set; }
        [field: SerializeField, Min(0)] public int PlayerMaxHealth { get; private set; }
        [field: SerializeField, Min(0)] public float PlayerInvincibleTime { get; private set; }

        [field: Header("Enemy"), SerializeField, Min(0)] public float EnemySpeed { get; private set; }
        [field: SerializeField, Min(0)] public float EnemyBorder { get; private set; }
        [field: SerializeField] public Vector2 EnemyRespawnPosition { get; private set; }

        [field: Header("Npc"), SerializeField, Range(1000, 10000)] public int NpcDelay { get; private set; }

        [field: Header("Projectile"), SerializeField, Min(0)] public int PoolCount { get; private set; }
        [field: SerializeField, Range(100, 1000)] public float Thrust { get; private set; }
        [field: SerializeField, Range(1000, 10000)] public int ProjectileLife { get; private set; }

        
        [field: Header("UiHealthBar"), SerializeField, Min(0)] public float DeathRate { get; private set; }
    }
}

