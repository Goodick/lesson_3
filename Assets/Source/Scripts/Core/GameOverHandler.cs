using TMPro;
using UniRx;
using UnityEngine;
using UnityEngine.UI;
using System;
using Game.GameObjects.Player;
using Game.Ui;
using Game.GameObjects.Enemy;
using Game.Core.Utils;

namespace Game.Core
{
    public class GameOverHandler : MonoBehaviour
    {
        [Header("Visual")]
        [SerializeField] private Image gameOverImage;
        [SerializeField] private TextMeshProUGUI numberDeath;
        [SerializeField] private AudioClip gameOverSound;
        [SerializeField] private ButtonInput buttonInput;

        [Header("Model")]
        [SerializeField] private PlayerController playerController;
        [SerializeField] private ReproductionAudio reproductionAudio;
        [SerializeField] private EnemyController enemyController;
        [SerializeField] private HealthScaler healthScaler;
        [SerializeField] private SavePrefs savePrefs;
        [SerializeField] private Data data;

        private bool isGameOver = false;

        private readonly Subject<int> onRestart = new();

        public bool IsGameOver => isGameOver;

        public IObservable<int> OnRestart => onRestart;

        private void OnEnable()
        {
            healthScaler.OnPlayerDeath.Subscribe(_ => OnGameOver()).AddTo(this);
            buttonInput.OnButtonClicked.Subscribe(_ => OnGameRestart()).AddTo(this);
        }

        private void OnGameRestart()
        {
            gameOverImage.gameObject.SetActive(false);
            buttonInput.gameObject.SetActive(false);

            playerController.Respawn();
            enemyController.Break();

            healthScaler.HPRecovery();

            ChangeGameState(false);
        }

        private void OnGameOver()
        {
            gameOverImage.gameObject.SetActive(true);
            buttonInput.gameObject.SetActive(true);

            savePrefs.SaveGame();
            var x = savePrefs.LoadGame;

            reproductionAudio.PlaySound(gameOverSound);

            numberDeath.text = x.ToString();
        }

        public void ChangeGameState(bool value)
        {
            isGameOver = value;
        }
    }
}

