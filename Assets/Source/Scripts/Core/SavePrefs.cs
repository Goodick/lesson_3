using UnityEngine;

namespace Game.Core
{
    public class SavePrefs : MonoBehaviour
    {
        [SerializeField] private GameOverHandler gameOverHandler;

        private readonly string intKey = "SavedInteger";
        private int numberDeath = 0;

        private void Start()
        {
            if (!PlayerPrefs.HasKey(intKey))
            {
                return;
            }

            numberDeath = PlayerPrefs.GetInt(intKey);
        }

        public void SaveGame()
        {
            numberDeath++;

            PlayerPrefs.SetInt(intKey, numberDeath);
            PlayerPrefs.Save();
        }

        public int LoadGame => PlayerPrefs.GetInt(intKey);

        public void ResetData()
        {
            PlayerPrefs.DeleteAll();
            numberDeath = 0;
        }
    }
}

