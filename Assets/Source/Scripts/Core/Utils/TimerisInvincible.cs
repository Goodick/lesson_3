using UnityEngine;
using Game.GameObjects.Player;

namespace Game.Core.Utils
{
    public class TimerisInvincible : MonoBehaviour
    {
        [SerializeField] private PlayerController rubyController;
        [SerializeField] private Data data;

        private bool isInvincible;
        private float invincibleTimer;

        public bool IsInvincible  => isInvincible; 
        public float InvincibleTimer  => invincibleTimer; 

        void Update()
        {

            if (IsInvincible)
            {
                invincibleTimer -= Time.deltaTime;

                if (InvincibleTimer < 0)
                    isInvincible = false;
            }
        }
        public void ReturnInvincible()
        {
            isInvincible = true;
            invincibleTimer = data.PlayerInvincibleTime;
        }
    }
}

