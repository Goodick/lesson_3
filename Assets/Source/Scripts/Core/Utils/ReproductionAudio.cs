using UnityEngine;

namespace Game.Core.Utils
{
    [RequireComponent(typeof(AudioSource))]
    public class ReproductionAudio : MonoBehaviour
    {
        [SerializeField] GameOverHandler gameOverHandler;

        private AudioSource audioSource;
        private void Start()
        {
            audioSource = GetComponent<AudioSource>();
        }
        public void PlaySound(AudioClip clip)
        {

            if (gameOverHandler.IsGameOver == true)
            {
                return;
            }

            audioSource.PlayOneShot(clip);
        }
    }
}

