namespace Game.Core.Utils
{
    public static class HealthData
    {
        public const int Decrement = -1;
        public const int Increment = 1;
        public const int Zero = 0;
    }
}

