using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Game.Core;

namespace Game.Ui
{
    public class UIHealthBar : MonoBehaviour
    {
        [Header("Visual")]
        [SerializeField] private Image bar;

        [Header("Model")]
        [SerializeField] private Data data;

        public void SetValue(float value)
        {
            bar.rectTransform.DOScaleX(value, data.DeathRate);
        }
    }
}
