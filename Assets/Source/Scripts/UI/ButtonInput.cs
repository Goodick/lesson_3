using System;
using UniRx;
using UnityEngine;

namespace Game.Ui
{
    public class ButtonInput : MonoBehaviour
    {
        private readonly Subject<Null> onButtonClicked = new();
        public IObservable<Null> OnButtonClicked => onButtonClicked;

        public void OnClick()
        {
            onButtonClicked.OnNext(null);
        }
    }
}

