using Game.Core;
using UnityEngine;
using Cysharp.Threading.Tasks;

namespace Game.GameObjects.NPC
{
    public class NpcCharacter : MonoBehaviour
    {
        [Header("Visual")]
        [SerializeField] private GameObject dialogBox;

        [Header("Model")]
        [SerializeField] private Data data;

        public async void DisplayDialogBox()
        {
            dialogBox.SetActive(true);
            await UniTask.Delay(data.NpcDelay);
            dialogBox.SetActive(false);
        }
    }
}

