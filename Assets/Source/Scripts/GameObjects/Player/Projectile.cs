using Game.GameObjects.Enemy;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Game.Core;

namespace Game.GameObjects.Player
{
    public class Projectile : MonoBehaviour
    {
        [SerializeField] private Data data;
        private Rigidbody2D rb2d;

        private void Awake()
        {
            rb2d = GetComponent<Rigidbody2D>();
        }

        public void LaunchProjectile(Vector2 direction, float force)
        {
            rb2d.AddForce(direction * force);
        }

        public void OnCollisionEnter2D(Collision2D other)
        {
            var enemyController = other?.collider.GetComponent<EnemyController>();
            enemyController?.Fix();
        }
    }
}
