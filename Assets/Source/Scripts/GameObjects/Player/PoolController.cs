using Game.Core;
using System.Collections.Generic;
using UnityEngine;

namespace Game.GameObjects.Player
{
    [RequireComponent(typeof(PlayerController))]
    public class PoolController : MonoBehaviour
    {
        [Header("Model")]
        [SerializeField] private Data data;
        [SerializeField] private Projectile projectilePrefab;

        private List<Projectile> projectiles = new();
        private PlayerController playerController;
        private ProjectilePool projectilePool;
        public ProjectilePool ProjectilePool  => projectilePool;

        private void Awake()
        {
            playerController = GetComponent<PlayerController>();

            for (var index = 0; index <= data.PoolCount; index++)
            {
                InstantiateProjectile();
            }

            projectilePool = new(projectiles);
        }

        private void InstantiateProjectile()
        {
            var projectileInstance = Instantiate(projectilePrefab, playerController.transform);
            projectileInstance.gameObject.SetActive(false);
            projectiles.Add(projectileInstance);
        }
    }
}
