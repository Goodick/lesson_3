using Game.Core;
using System.Collections.Generic;
using UnityEngine;

namespace Game.GameObjects.Player
{
    public class ProjectilePool
    {
        private List<Projectile> projectiles;

        public ProjectilePool(List<Projectile> projectiles)
        {
            this.projectiles = projectiles;
        }

        public Projectile SetProjectile(Vector2 position)
        {
            Projectile projectile = null;

            foreach (var index in projectiles)
            {
                if (!index.gameObject.activeInHierarchy)
                {
                    projectile = index;
                }
            }

            if (projectile is null)
            {
                projectile = projectiles[^1];
                projectiles.RemoveAt(projectiles.Count - 1);
                projectiles.Insert(0, projectile);
            }

            projectile.transform.position = position;
            projectile.transform.eulerAngles = Quaternion.identity.eulerAngles;
            projectile.gameObject.SetActive(true);
            return projectile;
        }
    }
}