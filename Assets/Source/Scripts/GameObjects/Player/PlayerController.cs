using Cysharp.Threading.Tasks;
using Game.Core;
using Game.Core.Utils;
using Game.GameObjects.NPC;
using UnityEngine;

namespace Game.GameObjects.Player
{
    [RequireComponent(typeof(Animator), typeof(Rigidbody2D), typeof(PoolController))]
    public class PlayerController : MonoBehaviour
    {
        [Header("Visual")]
        [SerializeField] private AudioClip throwSound;

        [Header("Model")]
        [SerializeField] private Data data;

        [SerializeField] private GameOverHandler gameOverHandler;
        [SerializeField] private SavePrefs savePrefs;
        [SerializeField] private ReproductionAudio sourceAudio;

        private Animator animator;
        private Rigidbody2D rb2d;
        private PoolController poolController;

        private Vector2 lookDirection = Vector2.right;

        private void Awake()
        {
            animator = GetComponent<Animator>();
            rb2d = GetComponent<Rigidbody2D>();
            poolController = GetComponent<PoolController>();
        }

        private void Update()
        {
            var lookX = "Look X";
            var lookY = "Look Y";
            var speed = "Speed";

            var horizontal = Input.GetAxis("Horizontal");
            var vertical = Input.GetAxis("Vertical");

            RaycastHit2D raycast = Physics2D.Raycast(Vector2.zero, Vector2.zero, 0f);
            NpcCharacter character;
            Vector2 moveDirection = new(horizontal, vertical);
            Vector2 position = rb2d.position;

            if (!Mathf.Approximately(moveDirection.x, 0.0f) || !Mathf.Approximately(moveDirection.y, 0.0f))
            {
                lookDirection.Set(moveDirection.x, moveDirection.y);
                lookDirection.Normalize();
            }

            animator.SetFloat(lookX, lookDirection.x);
            animator.SetFloat(lookY, lookDirection.y);
            animator.SetFloat(speed, moveDirection.magnitude);

            position.x += data.PlayerSpeed * horizontal * Time.deltaTime;
            position.y += data.PlayerSpeed * vertical * Time.deltaTime;

            rb2d.MovePosition(position);

            if (Input.GetKeyDown(KeyCode.C))
            {
                Launch();
            }

            if (Input.GetKeyDown(KeyCode.R))
            {
                savePrefs.ResetData();
            }

            if (Input.GetKeyDown(KeyCode.X))
            {
                raycast = Physics2D.Raycast(rb2d.position + Vector2.up * 0.2f, lookDirection, 1.5f, LayerMask.GetMask("NPC"));
            }

            if (raycast.collider is not null)
            {
                character = raycast.transform.GetComponent<NpcCharacter>();
                character?.DisplayDialogBox();
            }
        }

        public async void Launch()
        {
            var projectile = poolController.ProjectilePool.SetProjectile(transform.position);
            projectile.LaunchProjectile(lookDirection, data.Thrust);

            var trigger = "Launch";

            animator.SetTrigger(trigger);
            sourceAudio.PlaySound(throwSound);

            await UniTask.Delay(data.ProjectileLife);

            projectile.gameObject.SetActive(false);
        }

        public void Respawn()
        {
            rb2d.position = Vector2.zero;
        }
    }
}
