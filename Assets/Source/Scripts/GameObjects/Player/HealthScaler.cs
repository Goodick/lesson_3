using Game.Core;
using Game.GameObjects.Enemy;
using Game.GameObjects.Enviroment;
using Game.Core.Utils;
using System;
using System.Threading;
using UniRx;
using UnityEngine;
using Game.Ui;

namespace Game.GameObjects.Player
{
    public class HealthScaler : MonoBehaviour
    {
        [Header("Visual")]
        [SerializeField] private UIHealthBar healthBar;
        [SerializeField] private AudioClip hitSound;

        [Header("Model")]
        [SerializeField] private PlayerController rubyController;
        [SerializeField] private EnemyController enemyController;
        [SerializeField] private DamageCollectible damageCollectible;
        [SerializeField] private HealthCollectible healthCollectible;
        [SerializeField] private GameOverHandler gameOver;
        [SerializeField] private TimerisInvincible timer;
        [SerializeField] private ReproductionAudio reproductionAudio;
        [SerializeField] private Data data;

        private readonly Subject<Null> onPlayerDeath = new();

        public IObservable<Null> OnPlayerDeath => onPlayerDeath;


        private float currentHealth = HealthData.Zero;
        public float CurrentHealth => currentHealth;

        private void OnEnable()
        {
            currentHealth = data.PlayerMaxHealth;

            damageCollectible.OnPlayerDamage.Subscribe(_ => HP�hange(_)).AddTo(this);
            enemyController.OnPlayerDamage.Subscribe(_ => HP�hange(_)).AddTo(this);
            healthCollectible.OnPlayerHeal.Subscribe(_ => HP�hange(_)).AddTo(this);
            healthCollectible.OnPlayerHeal.Subscribe(_ => HP�hange(_)).AddTo(this);

            gameOver.OnRestart.Subscribe(_ => HP�hange(_)).AddTo(this);
        }

        private void HP�hange(int amount)
        {
            if (timer.IsInvincible)
            {
                return;
            }

            reproductionAudio.PlaySound(hitSound);

            currentHealth = Mathf.Clamp(CurrentHealth + amount, 0, data.PlayerMaxHealth);

            healthBar.SetValue(CurrentHealth / data.PlayerMaxHealth);
            timer.ReturnInvincible();

            if (CurrentHealth > 0)
            {
                return;
            }

            onPlayerDeath.OnNext(null);
            gameOver.ChangeGameState(true);
        }
        public void HPRecovery()
        {
            currentHealth = data.PlayerMaxHealth;

            healthBar.SetValue(1);
        }
    }
}



