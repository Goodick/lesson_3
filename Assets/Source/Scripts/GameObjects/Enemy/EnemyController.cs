using System;
using UniRx;
using UnityEngine;
using Cysharp.Threading.Tasks;
using Game.Core;
using Game.GameObjects.Player;
using Game.Core.Utils;

namespace Game.GameObjects.Enemy
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class EnemyController : MonoBehaviour
    {
        [Header("Visual")]
        [SerializeField] private ParticleSystem smokeEffect;

        [Header("Model")]
        [SerializeField] private HealthScaler healthScaler;
        [SerializeField] private GameOverHandler gameOverHandler;
        [SerializeField] private Data data;

        private Rigidbody2D rb2D;

        private Vector3 position = new();
        private float modify = 1f;
        private bool isFixed = false;

        private readonly Subject<int> onPlayerDamage = new();
        public IObservable<int> OnPlayerDamage => onPlayerDamage;

        private void Awake()
        {
            rb2D = GetComponent<Rigidbody2D>();
            position = rb2D.position;
        }
        private void Start()
        {
            _ = StartEnemyMovement();
        }

        private async UniTask StartEnemyMovement()
        {
            while (!isFixed)
            {
                Move();
                await UniTask.WaitUntil(() => Move() == position);
            }
        }

        private Vector3 Move()
        {
            var previousPosition = position;

            if (position.x >= data.EnemyBorder || position.x <= -data.EnemyBorder)
            {
                modify *= -1;
            }

            position.x += data.EnemySpeed * modify;
            rb2D.MovePosition(position);

            return previousPosition;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.GetComponent<PlayerController>())
            {
                return;
            }

            onPlayerDamage.OnNext(HealthData.Decrement);
        }

        public void Fix()
        {
            rb2D.simulated = false;
            isFixed = true;

            smokeEffect.Stop();
        }

        public void Break()
        {
            rb2D.simulated = true;
            rb2D.position = data.EnemyRespawnPosition;

            smokeEffect.Play();
        }
    }
}
