using Game.Core.Utils;
using Game.GameObjects.Player;
using System;
using UniRx;
using UnityEngine;

namespace Game.GameObjects.Enviroment
{
    public class DamageCollectible : MonoBehaviour
    {
        [SerializeField] private HealthScaler healthScaler;

        private readonly Subject<int> onPlayerDamage = new();
        public IObservable<int> OnPlayerDamage => onPlayerDamage;

        private void OnTriggerStay2D(Collider2D other)
        {
            if (!other.GetComponent<PlayerController>())
            {
                return;
            }

            onPlayerDamage.OnNext(HealthData.Decrement);
        }
    }
}

