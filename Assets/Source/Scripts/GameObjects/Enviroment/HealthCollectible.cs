using System;
using UniRx;
using UnityEngine;
using Game.GameObjects.Player;
using Game.Core;
using Game.Core.Utils;

namespace Game.GameObjects.Enviroment
{
    public class HealthCollectible : MonoBehaviour
    {
        [Header("Visual")]
        [SerializeField] private AudioClip collectedClip;
        [SerializeField] private Data data;

        [Header("Model")]
        [SerializeField] private HealthScaler healthScaler;
        [SerializeField] private ReproductionAudio sourceAudio;

        private readonly Subject<int> onPlayerHeal = new();
        public IObservable<int> OnPlayerHeal => onPlayerHeal;

        public void OnTriggerEnter2D(Collider2D other)
        {
            if (!other.GetComponent<PlayerController>())
            {
                return;
            }

            onPlayerHeal.OnNext(HealthData.Increment);
            sourceAudio.PlaySound(collectedClip);
            Destroy(this);
        }
    }
}
